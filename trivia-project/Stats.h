#pragma once

struct Stats
{
	float _timeForAnswer;
	int _rightAnswers;
	int _wrongAnswers;
	int _gamesPlayed;
};