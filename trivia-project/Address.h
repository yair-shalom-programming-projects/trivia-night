#pragma once

#include <string>


struct Address
{
	std::string _street,
				_apt,
				_city;
};