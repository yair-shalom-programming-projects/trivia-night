#pragma once

#include "IDatabase.h"
#include "sqlite3.h"
#include <sstream>


class SqliteDatabase : public IDatabase
{

public:

	// Constructors
	SqliteDatabase();
	virtual ~SqliteDatabase() = default;

	// Methods
	virtual bool doesUserExists(const std::string& username) override;
	virtual bool doesPasswordMatch(const std::string& username,
								   const std::string& password) override;

	virtual void addNewUser(const std::string& name,
							const std::string& password,
							const std::string& email, 
							const Address& address, 
							const std::string& phone,
							const std::string& date) override;

	// sqlite callback Methods
	static int booleanCallback(void* boolean, int len, char** data, char** columnName);

private: 
	
	bool openDatabase();
	void initializeDB();
	
	void sqliteRequest(const std::stringstream& sql, 
		int(*callback)(void*, int, char**, char**) = nullptr, void* data = nullptr);
	void sqliteRequest(const char* sql, 
		int(*callback)(void*, int, char**, char**) = nullptr, void* data = nullptr);

	
	sqlite3* _db;

};

