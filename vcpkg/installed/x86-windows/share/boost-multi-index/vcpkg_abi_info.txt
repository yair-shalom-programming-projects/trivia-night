boost-assert f0164d81d34d5445011c42ba83338e0a994480de
boost-bind 6d3253ed173f6db9c8944b13404a4e6d8b193730
boost-compatibility b8c066dd12592d501582c3fa90d40696203b78d9
boost-config 20cb5b4d1e7456b3d2a769884917011c84bf3510
boost-container-hash 655e70dba9df526383dc31c2807e4ccb934102a4
boost-core 5b621ead1d346332e9fb85a7c3d9a427a42dd19e
boost-detail 6f4d9b93ca309167fb6dc5f70a1f8ffd5da2442f
boost-foreach 7efb5250172cf136f8f1dd8e5ca352d537501e7c
boost-functional 041cbb6e6668e88ef4303df89ff21bc06d7a2114
boost-integer 6b8d4d12a8277a3c683643397afa14a29e2de1cd
boost-iterator fddea7c600d3de2090f1e176e21b6a1ee8cd1511
boost-move deec5e9edf50367896d76640d76a0cad2b46daa0
boost-mpl 65735cad3dbedf8253842731ec931d149995df8c
boost-preprocessor 37b102307036e11c67a31b1e33f4251cf64c80d2
boost-serialization 32c1568f02c7ac9da8f34c29823c0914c3e6fee8
boost-static-assert 9e3b351365925913118fbf9d59c082033cd3392b
boost-throw-exception 4d2d23bdb9e2ecb9531ea2c0c487af666626d37f
boost-tuple 3643db0af4570873eb3a192829ff1b289069a048
boost-type-traits 66409d9ca8e2e00de09f6d288d854ff5e8e31d58
boost-utility b6ff9991bc3dcf6748fc4136c4ad1c6d993d27d4
boost-vcpkg-helpers 2d54b6d10f1dc434d4c1ffdf6ffecb250df51316
cmake 3.20.2
features core
portfile.cmake 2cbfda6ca982e409f1d8c6ca8cf23c6db2189904
ports.cmake bd8d6584852d3c42c455b6794fc31eb6a4f0914a
post_build_checks 2
powershell 7.1.3
triplet x86-windows
triplet_abi 4c64db251cbf53c76a606595b52416ec991ab5bb-a1c0eabb0c5177b6a8fb97a58ae398880c47b352-d8c1982fc772c9b47c816273e96145953dc4d7ea
vcpkg.json f0417154f77dc23ec9416275296ea5b274aa1cfa
vcpkg_from_git 99e83c6ffac2dd07afaf73d795988ca5dec2dc8b
vcpkg_from_github a9dd1453b8873c9702731c6c36e79fb0ab6ae486
